#!/bin/bash
# --------------------------------------------------------------------
# COMPILING VIM FROM GIT REPOS WITH PYTHON36 SUPPORT ON DEBIAN STRETCH
# --------------------------------------------------------------------
# Inspired from https://www.xorpd.net/blog/vim_python3_install.html:w

sudo apt-get -y install checkinstall
sudo apt-get -y install sudo apt-get install python-dev python3-dev ruby ruby-dev
libx11-dev libxt-dev libgtk2.0-dev libncurses5 ncurses-dev

mkdir -p ~/tmp

cd ~/tmp/

git clone https://github.com/vim/vim.git

cd vim

git pull

./configure --enable-perlinterp --enable-python3interp=yes --enable-rubyinterp --enable-cscope --enable-gui=auto --enable-gtk2-check --enable-gnome-check --with-features=huge --enable-multibyte --with-x --with-compiledby="fredc" --with-python3-config-dir=/usr/local/lib/python3.6/config-3.6m-x86_64-linux-gnu/ --with-features=huge --with-tlib=ncursesw  --prefix=/usr/

sudo checkinstall
