#!/bin/bash
# --------------------------------------------------------------------
# COMPILING i3-gaps FROM GIT REPOS ON DEBIAN STRETCH
# --------------------------------------------------------------------

sudo apt-get -y install checkinstall
sudo apt-get -y  install libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev
libx11-dev libxt-dev libgtk2.0-dev libncurses5 ncurses-dev

mkdir -p ~/tmp

cd ~/tmp/

git clone https://www.github.com/Airblader/i3 i3-gaps

cd i3-gaps/

git checkout gaps-next

git pull

git checkout gaps-next

mkdir -p build

autoreconf --force --install
rm -rf build/
mkdir -p build && cd build/
../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
make
sudo checkinstall

